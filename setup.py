# from setuptools import setup
from distutils.core import setup, Extension

# from pip.req import parse_requirements
# from pip.download import PipSession

# install_reqs = parse_requirements('requirements.txt',session=PipSession())
# reqs = [str(ir.req) for ir in install_reqs]

setup(
    name="nodeflux.analytics",
    version='v4.1.2',
    packages=['nodeflux', 'nodeflux.analytics'],
    description='Nodeflux Object Congestion',
    url='https://bitbucket.org/verysmartdonkey/object-congestion',
    author=['Rizky Munggaran', 'Faris Rahman']
    # install_requires=reqs
)

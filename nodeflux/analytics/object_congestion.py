# author:kiki

from nodeflux.math_utils.math_function import *
from nodeflux.image_utils.image_function import *
from nodeflux.image_utils.polygon_drawer import PolygonDrawer
from nodeflux.analytics.base_analytics import BaseAnalytics
from scipy.spatial.distance import euclidean
import numpy as np
from nodeflux.tracker.sort import DetectAndTrack
import cv2
import threading
import time
import copy
import queue
import pytz
from datetime import datetime
from nodeflux.image_utils.watermark_logo import WatermarkLogo

TRACKER_CODES = ['sort', 'deepsort']
OBJECT_CENTER = ['bottom', 'center']

global_dump_values = None

dumping_queue = queue.Queue(maxsize=100)


class ObjectCongestion(BaseAnalytics):
    def __init__(self,
                 labels,
                 video_size,
                 maximum_frame_age=50,
                 time_threshold_second=3,
                 patch_size=None,
                 feature_name="AREA CONGESTION | property of nodeflux",
                 flag_show_timer=True,
                 flag_detect_area_only=True,
                 tracker_code='sort',
                 object_center=OBJECT_CENTER[1],
                 flag_watermark_logo=False,
                 rescale_bbox_factor=1,
                 code_nfdarknet='predictor',
                 frame_scale=1.,
                 dumping_interval_second=30):
        '''
        Args:
        ----

            :param labels: list of string, example : ['man','woman']
            :param video_size: tuple, (h,w)
            :param time_scale_factor: int, detected as object dwell after time_scale_factor. (second format)
            :param patch_size: tuple (H,w)
            :param spread_heatmap: int, 0-100 length to blur
            :param tracker_code (string)= code tracker- 'sort', 'deepsort'
            :param center_object='center' or 'bottom'

        example :
            labels = ['man', 'woman']
            maximum_frame_age=50,
             time_threshold_second=5,
             patch_size=None,
             feature_name="ILLEGAl PARKING RESTRICTION",
             flag_show_timer=True,
             flag_detect_area_only=True,
             tracker_code='sort', object_center='center'
        '''

        if tracker_code not in TRACKER_CODES:
            raise ValueError('You should choose one of tracker_codes. '
                             'Your tracker_code not in range \"' +
                             ','.join(TRACKER_CODES) + "\"")

        if object_center not in OBJECT_CENTER:
            raise ValueError('You should choose one of object_center. '
                             'Your object_center not in range \"' +
                             ','.join(OBJECT_CENTER) + "\"")

        self.dumping_interval_second = dumping_interval_second
        self.__frame_rescale_size = frame_scale
        ori_video_size = video_size
        video_size = np.array(video_size).astype(
            float) * self.__frame_rescale_size
        video_size = tuple(video_size.astype(int))

        if patch_size is None:
            p1 = int(float(video_size[1] / 720.) * float(9.))
            patch_size = (p1, p1)

        self.__grid = GridImage(
            patch_width=patch_size[0], patch_height=patch_size[1])

        if tracker_code == 'sort':

            # initialization using sort
            # -------
            self.tracker = DetectAndTrack(
                labels=labels,
                maximum_frame_age=maximum_frame_age,
                min_hits=0,
                min_confidence=0.05,
                nms=0.75,
                video_size=video_size,
                rescale_bbox_factor=rescale_bbox_factor,
                code=code_nfdarknet,
                frame_resize=self.__frame_rescale_size)

        else:
            raise ValueError('You should choose one of tracker_codes. '
                             'Your tracker_code not in range \"' +
                             ','.join(TRACKER_CODES) + "\"")
        '''initial random color'''
        self.__random_colors = np.random.randint(0, 255, (100, 3))
        self.__current_data = None
        self.__visualization_frame = None
        self.__original_frame = None
        self.__img_overlay = np.zeros((video_size[0], video_size[1], 3),
                                      np.uint8)

        self.__watermark_logo = None
        if flag_watermark_logo:
            self.__watermark_logo = WatermarkLogo()

        grid_size = np.zeros(
            (int(math.ceil(float(video_size[0]) / float(patch_size[0]))),
             int(math.ceil(float(video_size[1]) / float(patch_size[0])))))

        self.__object_properties = {}
        self.__object_properties['trajectories'] = {}
        self.__object_properties['history_dwell'] = {}
        self.__object_properties['history_pairing'] = {
        }  # 'historical movement object to another  area'
        self.__object_properties['average-labels'] = {}
        self.__object_properties['moving-status'] = {}  # idle/move

        self.rule_properties = {}
        self.rule_properties['roi-area'] = {}
        self.rule_properties['roi-names'] = {}
        self.rule_properties['tracker-code'] = tracker_code
        self.rule_properties['object-max-age'] = maximum_frame_age
        self.rule_properties['object-labels'] = labels
        self.rule_properties['object-center-bbox'] = object_center
        self.rule_properties['video-size'] = video_size
        self.rule_properties['global-ratio'] = video_size[0] / 720.
        self.rule_properties['patch-size'] = patch_size
        self.rule_properties['flag-show-timer'] = flag_show_timer
        self.rule_properties['time-threshold-second'] = time_threshold_second
        self.rule_properties['grid-size'] = grid_size.shape
        self.rule_properties['flag-detect-area_only'] = flag_detect_area_only
        self.rule_properties['feature-name'] = feature_name

        self.dump_properties = {}
        self.dump_properties['areas'] = {}
        self.dump_properties['preview'] = None
        self.dump_properties['timestamp'] = None
        self.__fps_start = time.time()

        self.__list_dumps = None

        # run dumping thread periodicallys
        self.__dump_data()

    def add_data(self, data, **kwargs):
        '''
               Procedure add data and frame, this procedure will process tracker and rule

               Args:
               ----

               :param data (dict):  produced by detector with json format :
                   Example :
                               {u'data': [{u'y2': 399, u'label': u'car', u'x2': 272, u'score': 0.21, u'y1': 345, u'x1': 223},
                                {u'y2': 431, u'label': u'person', u'x2': 531,
                                    u'score': 0.18, u'y1': 383, u'x1': 490},
                                {u'y2': 414, u'label': u'car', u'x2': 681, u'score': 0.16, u'y1': 380, u'x1': 616}], u'fps': 22.88}

               :param frame (numpy array): OPENCV format for an image

        '''
        self.__current_data = data

    def add_render_frame(self, frame=None):

        if frame is not None:
            self.__visualization_frame = frame.copy()
            self.__original_frame = frame.copy()

    def add_roi(self, roi, roi_name=None):
        '''
        Procedure Add the lines
        Args:
            roi : (list of points)
            example :

            roi = [(133, 349), (280, 572), (448, 452), (273, 256)]
            ..roi must be casted to np.array, its can be read by OPEN CV (POLYLINE)
        '''

        len_line = 0
        roi_area = self.rule_properties['roi-area']
        video_size = self.rule_properties['video-size']

        if len(roi_area.keys()) <= 0 and roi is None:
            roi = [[0, 0], [0, video_size[0] - 1],
                   [video_size[1] - 1, video_size[0] - 1],
                   [video_size[1] - 1, 0]]
            roi = np.array(roi).astype(float) * (
                1. / self.__frame_rescale_size)

        else:
            '''get last key of roi_areas, this to make sure that it have same key'''
            if len(roi_area.keys()) > 0:
                len_line = len(roi_area.keys()) + 1

        roi = np.array(roi).astype(float) * self.__frame_rescale_size
        roi = roi.astype(int).tolist()
        '''setup roi'''
        if roi_name is None:
            roi_name = 'AREA_' + str(len_line)
            self.rule_properties['roi-area'][len_line] = np.array([roi])
            self.rule_properties['roi-names'][len_line] = roi_name
            self.dump_properties['areas'][roi_name] = {}
            self.dump_properties['areas'][roi_name]['object_count'] = 0
            self.dump_properties['areas'][roi_name]['object_detected'] = []
            self.dump_properties['areas'][roi_name]['wait_durations'] = []
            self.dump_properties['areas'][roi_name]['max_wait_duration'] = 0
            self.dump_properties['areas'][roi_name]['dead_object_detected'] = [
            ]

        else:
            self.rule_properties['roi-area'][roi_name] = np.array([roi])
            self.rule_properties['roi-names'][roi_name] = roi_name
            self.dump_properties['areas'][roi_name] = {}
            self.dump_properties['areas'][roi_name]['object_count'] = 0
            self.dump_properties['areas'][roi_name]['object_detected'] = []
            self.dump_properties['areas'][roi_name]['wait_durations'] = []
            self.dump_properties['areas'][roi_name]['max_wait_duration'] = 0
            self.dump_properties['areas'][roi_name]['dead_object_detected'] = [
            ]

    def remove_roi(self, name):
        '''

        :param name (int): code of line based on number such as 0,1,2

        '''
        try:
            del self.rule_properties['roi-area'][name]
            del self.rule_properties['roi-names'][name]

        except Exception as ex:
            print(ex.message)

    def get_roi(self):
        return self.rule_properties['roi-area']

    def draw_roi(self, frame):
        '''
        Drawing new roi
        ---------------
        this process will show a frame to draw a new roi or more
        :return: point with new roi
        '''

        pd = PolygonDrawer('polygon')
        img_drw, points = pd.run(frame)

        print(points)
        ''' default ROI size is same as frame size'''
        if len(points) > 0:
            for i in range(len(points)):
                self.add_roi(points[i])
        else:
            self.add_roi(roi=None)

        return img_drw

    def process_data(self):
        '''
            Tracker
            :param:
                self.__current_data =dict
                Example :
                    json format example dict :
                    {u'data': [{u'y2': 381, u'label': u'man', u'x2': 608, u'score': 0.53, u'y1': 295, u'x1': 528},
                    {u'y2': 561, u'label': u'man', u'x2': 543, u'score': 0.89, u'y1': 369, u'x1': 453}], u'fps': 31.52}

            :returns
                Example :
                    tracking_results : [[ 844.  105.  921.  274.    1.]] # box
                    # id _label
                    dets_label : [0]
                    # id tracker still live
                    live_id_list : [0]
                    dead_id_list : []                                    # id tracker must be remove
                    # object confidence score
                    dets_score :  [ 0.53]
                    fps : 24.99                                          # FPS

                    # darkpy_result_selected is same with tracking_results
                    darkpy_result_selected : [[  8.44000000e+02   1.05000000e+02   9.21000000e+02   2.74000000e+02
                        5.30000000e-01   0.00000000e+00]]
        '''
        '''tracking the object '''

        if self.rule_properties['tracker-code'] == 'sort':

            tracking_results, \
                dets_label, \
                live_id_list, \
                dead_id_list, \
                dets_score, \
                fps, \
                darkpy_result_selected = self.tracker.process_json(
                    self.__current_data)
        else:
            raise ValueError('You should choose one of tracker_codes. '
                             'Your tracker_code not in range \"' +
                             ','.join(TRACKER_CODES) + "\"")

        date = time.time()
        fps = (float(1) / float(date - self.__fps_start))
        self.__fps_start = time.time()
        # print (self.fps)

        # rescale frame size
        self.__visualization_frame = resize_by_scale(
            self.__visualization_frame.copy(), self.__frame_rescale_size)
        ''''counting the object'''
        self.__process_rule(tracking_results, dets_label, dead_id_list, fps,
                            self.__visualization_frame.copy())

    ##
    # @brief      { function_description }
    ##
    # @param      self              The object
    # @param      frame             The frame
    # @param      tracking_results  The tracking results
    # @param      dead_id_list      The dead identifier list
    ##
    # @return     { description_of_the_return_value }
    ##
    def __process_rule(self,
                       tracker_result,
                       label_tracker_id,
                       dead_id,
                       fps,
                       frame=None):
        '''
        Example input params :
        -----------------
            tracking_results : [[ 844.  105.  921.  274.    1.]] # box
            # id _label
            label_tracker_id : [0]
            dead_id : []                                    # id tracker must be remove
            frame (numpy array) : an image OPENCV
        '''
        global global_dump_values
        self.fps = fps
        if self.fps == 0.0:
            self.fps = 15.0
        '''validate ROI if None or never added'''
        if len(self.rule_properties['roi-area']) == 0:
            self.add_roi(None)

        '''temporal counter'''
        __roi_counter = {}
        for id, roi in self.rule_properties['roi-area'].items():
            __roi_counter[id] = 0

        # clear dumping values so the list doesn't expand unwillingly
        for id_area, roi in self.rule_properties['roi-area'].items():
            self.dump_properties['areas'][id_area]['object_detected'] = []
            self.dump_properties['areas'][id_area]['wait_durations'] = []

        # '''
        #  INFO :
        # Drawing polylines
        # '''
        if frame is not None:
            for i, roi in (self.rule_properties['roi-area'].items()):
                # drawing polygon when object inside area
                cv2.polylines(frame, roi, True, (255, 255, 255), 2)

        __time_spent = datetime.now()

        for i in range(len(tracker_result)):

            obj = tracker_result[i]
            id_tracker = int(obj[4])
            '''fixing the values under 0 (<0)'''
            obj = [j if j > 0 else 0 for j in obj]

            # =====================================
            # --CALCULATING a center of rectangle
            # --obj[0] = x1, obj[1]=y1, obj[2]=x2, obj[3]=y2 (x1,x2,y1 & y2 = coordinate point)
            if self.rule_properties['object-center-bbox'] == OBJECT_CENTER[0]:
                x_mid, y_mid = calculate_object_center_bottom(obj)
            else:
                x_mid, y_mid = calculate_object_center(obj)
            ''' ADD new point into dictionary of the trajectory'''
            self.__add_trajectory(id_tracker, x_mid, y_mid, obj)
            '''Averaging labels name'''
            self.__object_properties[
                'average-labels'], id_label = most_occurrences_label_id(
                    id_tracker, int(label_tracker_id[i]),
                    self.__object_properties['average-labels'])
            color = self.__random_colors[id_label % 100, :]
            color = (int(color[0]), int(color[1]), int(color[2]))

            if frame is not None and \
                    not self.rule_properties['flag-detect-area_only']:
                # ================================================
                # --DRAWING cycle to the center of rectangle & object rectangle
                # ===================================================

                lbl_text = self.rule_properties['object-labels'][id_label]
                draw_rectangle_and_label_corner_box(
                    frame,
                    coordinates=obj,
                    color_rect_corner=color,
                    color_rect_overlay=color,
                    labels=['ID : ' + str(id_tracker), lbl_text])
                # # =========================================================================================
                # --Starting create OBJECT COUNTER
                # --set parameter count_trajectory
                frame = self.__draw_trajectory(
                    id_tracker=id_tracker,
                    color=color,
                    current_trajectory=[x_mid, y_mid],
                    img=frame,
                    trajectory=self.__object_properties['trajectories'])

            # ========================================================
            # --SeT Object INSIDE A ROI
            # ========================================================
            __trajectory = self.__object_properties['trajectories'][id_tracker]
            (x, y) = __trajectory[-1][0]
            in_roi_stats = False

            # check if trajectory is inside an area (checking every possible area)
            for id_area, roi in (self.rule_properties['roi-area'].items()):

                in_roi_stats = check_if_point_inside_roi((x, y), roi)

                if in_roi_stats:
                    # count object when object is inside
                    grid_pos_current = __trajectory[-1][1]
                    '''
                     Generate new neighbor / neighbor position while the object moving position
                     OR change  ROI (AREA ID)
                     '''

                    __his_dwell = self.__object_properties['history_dwell']

                    if id_tracker not in __his_dwell or \
                            __his_dwell[id_tracker]['id-area'] != id_area:
                        __his_dwell[id_tracker] = {}
                        __his_dwell[id_tracker]['time-in'] = __time_spent
                        __his_dwell[id_tracker]['time-out'] = __time_spent
                        __his_dwell[id_tracker]['gridpos'] = grid_pos_current
                        __his_dwell[id_tracker][
                            'gridpos-neighbor'] = self.__generate_neighborhood(
                                id_tracker, grid_pos_current)
                        __his_dwell[id_tracker]['id-area'] = id_area
                        __his_dwell[id_tracker]['frame-count'] = 1
                        __his_dwell[id_tracker]['flag-counted'] = False

                        # __his_dwell[id_tracker]['pairing-id'] = []
                        __his_dwell[id_tracker][
                            'class'] = self.rule_properties['object-labels'][
                                id_label]

                        ori_bbox = (np.array(obj[:4]) * (
                            1. / self.__frame_rescale_size)).astype(int)
                        __his_dwell[id_tracker]['image'] = crop_point(
                            self.__original_frame, ori_bbox[0], ori_bbox[1],
                            ori_bbox[2], ori_bbox[3])

                        if id_tracker not in self.__object_properties[
                                'history_pairing']:
                            self.__object_properties['history_pairing'][
                                id_tracker] = []
                    '''check when a grid-pos as a members of neighbor, If not a member will generate new neighbor
                    for fixing significant moving point in an object
                    '''
                    if grid_pos_current not in __his_dwell[id_tracker][
                            'gridpos-neighbor']:
                        __his_dwell[id_tracker][
                            'gridpos-neighbor'] = self.__generate_neighborhood(
                                id_tracker, grid_pos_current)
                        __his_dwell[id_tracker]['gridpos'] = grid_pos_current
                    '''update time-end and weight at current grid-pos based on id_tracker'''
                    __his_dwell[id_tracker]['time-out'] = __time_spent
                    __his_dwell[id_tracker]['frame-count'] += 1.

                    delta_time = (
                        __time_spent -
                        __his_dwell[id_tracker]['time-in']).total_seconds()

                    __his_dwell[id_tracker]['duration'] = delta_time

                    # draw dwelling duration info if timer exceeds threshold
                    if delta_time >= self.rule_properties[
                            'time-threshold-second']:
                        if __his_dwell[id_tracker]['flag-counted'] == False:
                            __his_dwell[id_tracker]['flag-counted'] = True
                        '''count the object who is dwell inside ROI'''
                        __roi_counter[id_area] += 1
                        '''Drawing counter time in top of object detected'''
                        if frame is not None and self.rule_properties[
                                'flag-show-timer']:
                            '''call a function to calculate timer'''
                            lbl = self.__calculate_time(id_tracker)

                            frame = draw_rectangle_and_label_corner_box(
                                frame,
                                coordinates=obj,
                                color_rect_corner=color,
                                color_rect_overlay=color,
                                transparency_val=0.6,
                                labels=[
                                    'ID : ' + str(id_tracker), self.
                                    rule_properties['object-labels'][id_label],
                                    lbl
                                ])

                            # # =========================================================================================
                            # --Starting create OBJECT COUNTER
                            # --set parameter count_trajectory
                            frame = self.__draw_trajectory(
                                id_tracker=id_tracker,
                                color=color,
                                current_trajectory=[x_mid, y_mid],
                                img=frame,
                                trajectory=self.
                                __object_properties['trajectories'])
                    '''Drawing BBOX'''
                    if frame is not None and self.rule_properties[
                            'flag-detect-area_only']:
                        # ================================================
                        # --DRAWING cycle to the center of rectangle & object rectangle
                        # ===================================================

                        lbl_text = self.rule_properties['object-labels'][
                            id_label]

                        frame = draw_rectangle_and_label_corner_box(
                            frame,
                            coordinates=obj,
                            color_rect_corner=color,
                            color_rect_overlay=color,
                            labels=['ID : ' + str(id_tracker), lbl_text])
                        # # =========================================================================================
                        # --Starting create OBJECT COUNTER
                        # --set parameter count_trajectory
                        frame = self.__draw_trajectory(
                            id_tracker=id_tracker,
                            color=color,
                            current_trajectory=[x_mid, y_mid],
                            img=frame,
                            trajectory=self.
                            __object_properties['trajectories'])

                    self.__object_properties['history_dwell'] = __his_dwell

                    break

        if frame is not None:
            '''Drawing Counter Box and Polygon (ROI)'''

            for id, roi in (self.rule_properties['roi-area'].items()):
                if __roi_counter[id] > 0:
                    cv2.polylines(frame, roi, True, (0, 0, 255),
                                  2)  # draw area

                    # set posotion on frame
                    x = int(min(roi[0][:, [0]]))
                    y = int(int(min(roi[0][:, [1]]) + max(roi[0][:, [1]])) / 2)

                    lbl_size, think = get_font_size(
                        rasio=self.rule_properties['global-ratio'])
                    lbl = 'COUNT OF OBJECT : ' + str(__roi_counter[id])

                    '''
                    DUMPING DATA FOR OBJECT-CONGESTION RULE
                    '''
                    # add counted object information inside area roi
                    self.dump_properties['areas'][id][
                        'object_count'] = __roi_counter[id]

                    # add dump_properties to corresponding area id/area name
                    for item in self.__object_properties['history_dwell']:
                        if self.__object_properties['history_dwell'][item][
                                'id-area'] == id:
                            self.dump_properties['areas'][id][
                                'object_detected'].append(self.__object_properties[
                                    'history_dwell'][item])
                            self.dump_properties['areas'][id]['wait_durations'].append(self.__object_properties[
                                'history_dwell'][item]['duration'])

                    # add frame timestamp to dumping properties
                    self.dump_properties['timestamp'] = str(
                        datetime.now(pytz.utc).isoformat())

                    # cv2.rectangle(frame, (x - 5, y - 45), (x + (len(lbl) * 10), y + 10), (0, 0, 0), -1)
                    # print ('==> ',x,y , x - 5, y - 45, int(x + (len(lbl) * (lbl_size * 2) * 10)), y + 10)
                    cv2.rectangle(
                        frame, (x - 5, y - 45),
                        (x + int(len(lbl) * (lbl_size * 2) * 10), y + 10),
                        (0, 0, 0), -1)

                    cv2.putText(frame, self.rule_properties['roi-names'][id],
                                (x, y - 25), cv2.FONT_HERSHEY_SIMPLEX,
                                lbl_size, (14, 255, 10), think)
                    cv2.putText(frame, lbl, (x, y), cv2.FONT_HERSHEY_SIMPLEX,
                                lbl_size, (14, 255, 10), think)

        self.__visualization_frame = frame.copy()
        self.dump_properties['preview'] = frame.copy()

        '''dump data when id_tracker disappear, but was counted before'''
        if len(dead_id) > 0:
            for id, roi in (self.rule_properties['roi-area'].items()):
                for obj in self.__object_properties[
                        'history_dwell']:  # --clear dect historical center ponts has detected based on counter id
                    if obj in dead_id and self.__object_properties['history_dwell'][obj][
                            'id-area'] == id:
                        self.dump_properties['areas'][id][
                            'dead_object_detected'].append(self.__object_properties[
                                'history_dwell'][obj])
                        self.dump_properties['areas'][id]['wait_durations'].append(self.__object_properties[
                            'history_dwell'][obj]['duration'])

            # remove dumped dead_id
            self.__remove_trajectory_history(dead_id)

        global_dump_values = copy.deepcopy(self.dump_properties)

    def __generate_neighborhood(self, id_tracker, grid_pos_current):
        '''while the object moving but still inside  ROI
                     example : grid-pos (2,2)
                             grid_pos_neighbor =
                             [[2, 2], [2, 3], [2, 4],
                             [3, 2], -- [3, 3],
                             [3, 4], [4, 2], [4, 3], [4, 4]]

                             the center is index of 4
    '''
        (x, y) = self.__object_properties['trajectories'][id_tracker][-1][0]
        grid_pos_neighbor = [grid_pos_current]
        grid_pos_temp = (grid_pos_current[0] - 1, grid_pos_current[1] - 1)
        for i in range(3):
            for j in range(3):
                middle_patch = self.__grid.get_pixel_from_center_of_patch(
                    grid_pos_temp[0] + i, grid_pos_temp[1] + j)
                if euclidean((x, y), (middle_patch)) <= self.rule_properties['patch-size'][0] and \
                        (grid_pos_temp[0] + i, grid_pos_temp[1] + j) not in grid_pos_neighbor:
                    b = (grid_pos_temp[0] + i, grid_pos_temp[1] + j)
                    grid_pos_neighbor.append(b)

        return grid_pos_neighbor

    def __calculate_time(self, id_tracker):
        '''
        args :
            :param id_tracker: int
            :param grid_pos: tuple (x,y) in a grid

        return:
            string of time (timer)
        '''
        delta_time = self.__object_properties['history_dwell'][id_tracker][
            'time-out'] - self.__object_properties['history_dwell'][
                id_tracker]['time-in']

        seconds = delta_time.total_seconds()
        h = seconds // 3600
        m = (seconds % 3600) // 60
        s = int(seconds % 60)

        time_str = str(int(h)) + ':' + str(int(m)) + ':' + str(s)

        lbl = 'TIMER :' + time_str + ' sec'

        return lbl

    def __dump_data(self):
        '''
        dump data
        -----------
        the data will be saved when the object (person) moved  from  dwell area

        args :
            repeated : boolean
        '''
        global dumping_queue
        local_dump_values = copy.deepcopy(global_dump_values)
        # execute dumping function every n seconds
        threading.Timer(self.dumping_interval_second,
                        self.__dump_data).start()

        # attribute['attribute']['image'] = self.__object_properties[
        #     'history_dwell'][id_tracker]['image']

        # ask kiki??
        # self.__object_properties['history_pairing'][id_tracker].append(
        #     id_area)

        # add all duration from detected object, including wait time from dead object
        for id, roi in (self.rule_properties['roi-area'].items()):
            for obj in local_dump_values['areas'][id]['object_detected']:
                local_dump_values['areas'][id]['wait_durations'].append(
                    obj['duration'])
            for obj in local_dump_values['areas'][id]['dead_object_detected']:
                local_dump_values['areas'][id]['wait_durations'].append(
                    obj['duration'])

            # get max wait duration from all object in the ROI
            local_dump_values['areas'][id]['max_wait_duration'] = max(
                local_dump_values['areas'][id]['wait_durations'])

            del local_dump_values['areas'][id]['dead_object_detected']
            del local_dump_values['areas'][id]['object_detected']
            del local_dump_values['areas'][id]['wait_durations']

        dumping_queue.put(local_dump_values)

        # # append ready-dump data to list of dump
        # self.__list_dumps = local_dump_values

        # clear DEAD OBJECTS only if their duration properties already dumped
        for id_area, roi in self.rule_properties['roi-area'].items():
            self.dump_properties['areas'][id_area]['dead_object_detected'] = [
            ]

    def __remove_trajectory_history(self, dead_id):
        # ======================================================
        # ///Research only for clear temporary data (proposed for clean memory)

        if len(dead_id) > 0:

            for k in dead_id:  # --clear dect historical center ponts has detected based on counter id
                # --//remove trajectory
                del self.__object_properties['trajectories'][k]

                if k in self.__object_properties['history_dwell']:
                    del self.__object_properties['history_dwell'][k]

                if k in self.__object_properties['average-labels']:
                    del self.__object_properties['average-labels'][k]
                # print ('delete ->', k, self.__object_properties['moving-status'])
                if k in self.__object_properties['moving-status']:
                    del self.__object_properties['moving-status'][k]

                if k in self.__object_properties['history_pairing']:
                    del self.__object_properties['history_pairing'][k]

    def __draw_trajectory(self, img, id_tracker, trajectory, color,
                          current_trajectory):
        # =======================================================
        # ///DRAWING shape in the object has detected
        # =======================================================
        # --////Drawing the trajectory on the object
        # --//// activate to see the trajectory
        if len(trajectory[id_tracker]
               ) > 2:  # checking a list of point with min 2 point
            # --drawing trajectory with the straight line
            # #=================================================
            # //// if you want to not use the straight line, use this code
            thickness = 4
            line = [
                list(np.array(x[0]).flatten()) for x in trajectory[id_tracker]
            ]
            line.append(current_trajectory)
            distance = euclidean(line[-thickness::][0], line[-thickness::][-1])

            if int(distance) <= thickness:
                return img

            try:
                line = interpolate_points(
                    np.array(line[-thickness::]), int(distance),
                    2).astype(np.uint32)
                step = 3
                for c in (reversed(range(0, len(line), step))):
                    try:
                        cv2.line(img, (line[c][0], line[c][1]),
                                 (line[c - step][0], line[c - step][1]), color,
                                 int(thickness))
                        thickness -= 1
                        if thickness <= 0:
                            break
                    except:
                        pass

            except:
                pass
        return img

    def __add_trajectory(self, id_tracker, x_mid, y_mid, bbox):
        # --// add the mid point as a trajectory an object detected
        # -- only unique mid point will be save

        def __generate_neighborhood(x, y, grid_pos_current, patch_size):
            '''while the object moving but still inside  ROI
                         example : grid-pos (2,2)
                                 grid_pos_neighbor =
                                 [[2, 2], [2, 3], [2, 4],
                                 [3, 2], -- [3, 3],
                                 [3, 4], [4, 2], [4, 3], [4, 4]]

                                 the center is index of 4
        '''

            grid_pos_neighbor = [grid_pos_current]
            grid_pos_temp = (grid_pos_current[0] - 1, grid_pos_current[1] - 1)
            for i in range(3):
                for j in range(3):
                    middle_patch = self.grid.get_pixel_from_center_of_patch(
                        grid_pos_temp[0] + i, grid_pos_temp[1] + j)
                    if euclidean((x, y), (middle_patch)) <= patch_size[0] / 2 and \
                            (grid_pos_temp[0] + i, grid_pos_temp[1] + j) not in grid_pos_neighbor:
                        b = (grid_pos_temp[0] + i, grid_pos_temp[1] + j)
                        grid_pos_neighbor.append(b)

            return grid_pos_neighbor

        position = self.__grid.get_grid_position((x_mid, y_mid))
        trajectory = self.__object_properties['trajectories']
        # neighborhood=self.object_properties['neighborhood']

        if id_tracker not in trajectory:

            # self.object_properties['neighborhood'][id_tracker]=  generate_neighborhood(x_mid,y_mid, position, self.rule_properties['patch-size'])
            self.__object_properties['trajectories'][id_tracker] = [((x_mid,
                                                                      y_mid),
                                                                     position)]
            # self.object_properties['moving-status'][id_tracker]=['IDLE','IDLE']

            # self.object_properties['moving-status-compare'][id_tracker] = [-1] #** riset memed
            self.__object_properties['moving-status'][id_tracker] = [-1]

        elif len(trajectory[id_tracker]) > 0:
            '''checking neighborhood position
            while the current position around the neighbor, ignored
            '''

            # ** Riset memed
            # ===================

            # tmp= np.asarray(dict(trajectory[id_tracker]).keys()[-5::])
            # # print  np.average(tmp, axis=0)
            # print dict(trajectory[id_tracker]).keys()[-5::]
            # tmp= np.average(tmp, axis=0).astype(int)
            # print 'avg_pixel :',tmp
            # tmp_pos= self.grid.get_grid_position(tuple(list(tmp)))
            # tmp_pos= tuple(list(tmp_pos))
            # print 'avg_grid:', id_tracker, tmp_pos, position
            #
            # # print np.asarray(dict(trajectory[id_tracker]).values())
            # # print  id_tracker,tmp_pos, position, tmp_pos== position
            # if position == tmp_pos:
            # if euclidean((x_mid, y_mid), (self.object_properties['neighborhood'])) <= patch_size[0] / 2

            # using moving weighted

            last_trajec = np.asarray(trajectory[id_tracker][-10::])[:, 0, :]
            w = len(last_trajec)
            wb = 0
            t = 0
            for p in last_trajec:
                wb += p * w
                t += w
                w -= 1
            wb = wb / t

            distance_center_wb = euclidean(tuple(wb), (x_mid, y_mid))
            distance_radius = self.rule_properties['patch-size'][0]

            obj_width = bbox[2] - bbox[0]
            obj_height = bbox[3] - bbox[1]
            bbox_min = min(obj_height, obj_width)
            '''rasio radius changes'''
            if distance_radius >= bbox_min * 0.25:
                distance_radius = distance_radius / 2
                # print distance_radius, bbox_min

            # End riset memed
            # ===========================

            # if position in dict(trajectory[id_tracker]).values()[-5::]:
            # if position == trajectory[id_tracker][-2::][0][-1]:
            # if position in neighborhood[id_tracker]:
            '''remove historical moving status'''
            # if len(self.object_properties['moving-status'][id_tracker])>10:
            threshold_length = 15
            self.__object_properties['moving-status'][
                id_tracker] = self.__object_properties['moving-status'][
                    id_tracker][-threshold_length - 1::]
            '''using radius'''
            if distance_center_wb < distance_radius:
                self.__object_properties['moving-status'][id_tracker].append(
                    -1)
                # self.object_properties['moving-status'][id_tracker].append('IDLE')
                # self.object_properties['moving-status'][id_tracker].pop(0)

                # return trajectory

            else:
                self.__object_properties['moving-status'][id_tracker].append(1)
                # self.object_properties['moving-status'][id_tracker].append('MOVE')
                # self.object_properties['moving-status'][id_tracker].pop(0)

                (x, y) = trajectory[id_tracker][-1][0]
                x_new = int((x + x_mid) / 2)
                y_new = int((y + y_mid) / 2)

                position_new = self.__grid.get_grid_position((x_new, y_new))

                distance = euclidean(trajectory[id_tracker][-1][1],
                                     position_new)
                t_min = 2.
                if int(distance) > t_min:

                    lines = interpolate_points(
                        np.array((trajectory[id_tracker][-1][1],
                                  position_new)),
                        int(distance) + 1, 1).astype(np.uint32)

                    for point in lines:
                        x_, y_ = self.__grid.get_pixel_from_center_of_patch(
                            point[0], point[1])
                        trajectory[id_tracker].append(((x_, y_), tuple(point)))

                else:

                    if len(trajectory[id_tracker]) == 1:
                        trajectory[id_tracker].append(((x_new, y_new),
                                                       position_new))
                    else:
                        trajectory[id_tracker][len(trajectory[id_tracker]) -
                                               1] = ((x_new, y_new),
                                                     position_new)

                trajectory[id_tracker].append(((x_mid, y_mid), position))

                # neighborhood[id_tracker] = __generate_neighborhood(x_mid, y_mid, position,
                #                                                                      self.rule_properties[
                #                                                                          'patch-size'])

        self.__object_properties['trajectories'] = trajectory
        # self.object_properties['neighborhood']=neighborhood

    def result_render_frame(self):

        if self.__visualization_frame is None:
            return None

        # self.__grid.draw_grid(frame)

        normal_font_size, normal_font_thin = get_font_size(
            self.rule_properties['global-ratio'])

        add_h = int(40 * self.rule_properties['global-ratio'])

        img_window_border = np.zeros(
            (self.__visualization_frame.shape[0] + add_h,
             self.__visualization_frame.shape[1], 3), np.uint8)

        label = self.rule_properties['feature-name']
        rasio_font_size = math.ceil(15 * self.rule_properties['global-ratio'])
        rasio_font_size = int(rasio_font_size)

        xrect = int(rasio_font_size + (
            (len(label) * (normal_font_size * 2)) * 10))

        cv2.rectangle(self.__visualization_frame, (0, 0),
                      (xrect, rasio_font_size * 3), (255, 255, 255), -1)

        cv2.putText(self.__visualization_frame, label, (int(
            10 * self.rule_properties['global-ratio']), rasio_font_size * 2),
            cv2.FONT_HERSHEY_SIMPLEX, normal_font_size, (0, 0, 0),
            normal_font_thin)

        img_window_border[add_h:add_h + self.__visualization_frame.
                          shape[0], 0:self.__visualization_frame.
                          shape[1]] = self.__visualization_frame

        output = resize_by_custom(img_window_border,
                                  self.__visualization_frame.shape[1],
                                  self.__visualization_frame.shape[0])
        '''create watermark logo'''
        if self.__watermark_logo is not None:
            output = self.__watermark_logo.draw_watermark(output)

        return output

    def result_data(self):
        '''
        Get s list if dictionary

        # example output :
        # =====================================
        #
        #  [
        #    {'attribute':
        #     {
        #     'time_in': datetime.datetime(2018, 10, 30, 13, 57, 3, 975046),
        #     'id_tracker': 1, 'duration': 11.087064,
        #      'roi_name': 'P-F',
        #      'time_out': datetime.datetime(2018, 10, 30, 13, 57, 15, 62110),
        #      'class': 'man'
        #      }
        #    }
        #  ]
        #

        :return: list of dict
        '''

        # if self.__list_dumps == None:
        #     return None
        # else:
        #     return self.__list_dumps
        if not dumping_queue.empty():
            return dumping_queue.get()
